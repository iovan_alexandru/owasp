﻿using OWASP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using OWASP.Utils;
using System.Diagnostics;

namespace OWASP.Repository
{
    public class UserRepository
    {
        public User LoginUserBad(string email, string password)
        {
            User user = new User();
            using (SqlConnection connection = new SqlConnection(ConfigParser.ConnectionString))
            {
                IEnumerable<User> users = null;
                try
                {
                    connection.Open();
                    users = connection.Query<User>(string.Format("select * from Users where Email = '{0}' and Password = '{1}'", email, password));
                }
                catch (SqlException ex)
                {
                    Debug.Fail(ex.ToString(), ex.Message);
                }

                if (users != null && users.Count() > 0)
                {
                    return users.First();
                }
            }

            return null;
        }

        public User LoginUserGood(string email, string password)
        {
            User user = new User();
            using (SqlConnection connection = new SqlConnection(ConfigParser.ConnectionString))
            {
                IEnumerable<User> users = null;
                try
                {
                    connection.Open();
                    users = connection.Query<User>("select * from Users where Email = @em and Password = @pass", new { em = email, pass = password });
                }
                catch (SqlException ex)
                {
                    Debug.Fail(ex.ToString(), ex.Message);
                }

                if (users != null && users.Count() > 0)
                {
                    return users.First();
                }
            }

            return null;
        }

        public void RegisterUser(User user)
        {
            using (SqlConnection connection = new SqlConnection(ConfigParser.ConnectionString))
            {
                try
                {
                    connection.Open();
                    connection.Execute(string.Format("insert into Users VALUES ('{0}', '{1}', '{2}', '{3}')", user.Email, user.Name, user.Password, UserRoles.Default.ToString()));
                }
                catch (SqlException ex)
                {
                    throw new RepositoryException("Unknown error occured.", ex);
                }
            }
        }

        public User GetUser(string email)
        {
            User user = new User();
            using (SqlConnection connection = new SqlConnection(ConfigParser.ConnectionString))
            {
                IEnumerable<User> users = null;
                try
                {
                    connection.Open();
                    users = connection.Query<User>(string.Format("select * from Users where Email = '{0}' ", email));
                }
                catch (SqlException ex)
                {
                    Debug.Fail(ex.ToString(), ex.Message);
                }

                if (users != null && users.Count() > 0)
                {
                    return users.First();
                }
            }

            return null;
        }
    }
}
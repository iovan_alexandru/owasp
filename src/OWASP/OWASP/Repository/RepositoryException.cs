﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace OWASP.Repository
{
    [Serializable]
    public class RepositoryException : Exception, ISerializable
    {
        public RepositoryException() { }
        public RepositoryException(string message) : base(message) { }
        public RepositoryException(string message, Exception innerException) : base(message, innerException) { }
        public RepositoryException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}
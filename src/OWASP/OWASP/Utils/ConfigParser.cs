﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace OWASP.Utils
{
    public class ConfigParser
    {
        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["owaspDB"].ConnectionString;
            }
        }
    }
}
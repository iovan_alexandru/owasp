﻿using OWASP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OWASP.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        protected UserRepository UserRepository { get; set; }

        public HomeController()
        {
            this.UserRepository = new UserRepository();
        }

        public ActionResult Index()
        {
            Models.User user = this.UserRepository.GetUser(User.Identity.Name);
            if (user == null)
            {
                return RedirectToAction("Logout", "Authentication");
            }

            ViewBag.Name = user.Name;
            ViewBag.Role = user.Role;

            return View();
        }
    }
}
﻿using OWASP.Models;
using OWASP.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OWASP.Controllers
{
    public class AuthenticationController : Controller

    {
        protected UserRepository UserRepository { get; set; }

        public AuthenticationController()
        {
            UserRepository = new UserRepository();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            ModelState.Remove("Name");
            if (ModelState.IsValid)
            {
                try
                {
                    User loggedUser = this.UserRepository.LoginUserGood(user.Email, user.Password);
                    if (loggedUser != null)
                    {
                        FormsAuthentication.SetAuthCookie(loggedUser.Email, false);
                        return RedirectToAction("Index", "Home");
                    }

                    ModelState.AddModelError("", "Invalid credentials");
                }
                catch (RepositoryException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View(user);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }

            try
            {
                this.UserRepository.RegisterUser(user);
            }
            catch (SqlException)
            {
                ModelState.AddModelError("", "Could not register user.");
            }

            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}